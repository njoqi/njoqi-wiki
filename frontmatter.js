// Function to parse frontmatter
export default function parseFrontmatter(input) {
    const parts = input.replace(/\r/g, "").split(/\n/);
    const lines = parts.filter((el) => el);
    const frontmatter = {};

    let parentLine = "";
    
    lines.forEach((line, index) => {
        
        const halves = line.split(":");
        if (halves.length > 2) {
            halves[1] = halves.slice(1).join(":");
        }

        if (halves.length > 1) {
            if (!halves[1]) {
                parentLine = halves[0];
                frontmatter[halves[0]] = {};
            }
            else if (halves[0].startsWith("  ")) {
                frontmatter[parentLine][halves[0].slice(3).trim()] = halves[1].trim();
            }
            else {
                frontmatter[halves[0]] = halves[1].trim();
            }
        }
    });
    
    return frontmatter;
}