// BUILDER - Build the website from Markdown and Whiskers templates
import mime from "mime-types";
import fs from "fs";
import path from "path";
import crypto from "crypto";
import rimraf from "rimraf";
import parseFrontmatter from "./frontmatter.js";
import renderTemplate from "./templating.js";
import renderMarkdown from "./markdown.js";

// Function to find all files recursively in subfolders
const recursiveReaddir = function(dirPath, arrayOfFiles = [] ) {
  const files = fs.readdirSync(dirPath);

  files.forEach(function(file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = recursiveReaddir(dirPath + "/" + file, arrayOfFiles);
    } else {
      const newFile = path.join(dirPath, "/", file);
      arrayOfFiles.push(newFile);
    }
  });

  return arrayOfFiles;
};

export default function build(config, cb, buildMode = "prod") {

  // Remove existing /dist files
  rimraf("./dist/*", {glob: { nosort: true, silent: true, dot: false }}, () => {
      // Construct today's date
      const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ];
      const todayTimestamp = Date.now();
      const today = new Date(todayTimestamp);
      const [todayMonth, todayDate, todayYear] = today.toLocaleDateString("en-US").split("/");
      const lastUpdated = todayDate + " " + months[parseInt(todayMonth) - 1] + " " + todayYear;
  
      // Build the CSS
      console.log("\x1b[32m", "Compiling CSS...", "\x1b[0m");

      let cssPath = "";
      let css = fs.readFileSync(config.stylesheet, "utf-8");

      // Remove comments
      css = css.replace(/(\/\*.*?\*\/)/gms, () => "");

      // Resolves imports in main file
      css = css.replace(/(?:^@import "(.*?)";)/gm, (match, ...args) => {
        if(args[0]) {
          return fs.readFileSync(`./src/styles/${args[0]}`, "utf-8").replace(/(\/\*.*?\*\/)/gms, () => "");
        }
        return "";
      });

      // Remove indentation
      css = css.replace(/^[ \t]+/gm, '')

      // Remove newlines
      css = css.replace(/(\r?\n)/g, () => "");
  
      if (buildMode === "prod") {
        
        // Generate hash of the CSS file
        const hashSum = crypto.createHash('md5');
        hashSum.update(css);
        const cssHash = hashSum.digest('hex');
  
        // Output the CSS file
        cssPath = `/assets/style.${cssHash}.css`;

        fs.mkdir(path.dirname(`./dist${cssPath}`), {recursive: true}, (err) => {
          if (err) throw err;
          fs.writeFileSync(`./dist${cssPath}`, css);
        });
      }
      else {

        // Output the CSS file
        fs.mkdir(path.dirname("./dist/assets/.style.dev.css"), {recursive: true}, (err) => {
          if (err) throw err;
          fs.writeFileSync("./dist/assets/.style.dev.css", css);
        });
      }
  
      // Function to turn page names into slugs
      const slugify = (str) => {
      return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '') // Remove accents
        .replace(/([^\w]+|\s+)/g, '-') // Replace space and other characters by hyphen
        .replace(/\-\-+/g, '-')	// Replaces multiple hyphens by one hyphen
        .replace(/(^-+|-+$)/g, '') // Remove extra hyphens from beginning or end of the string
        .toLocaleLowerCase(); // Normalise case
      }
  
      // Function to map wikilinks to the proper url
      // We store the pages in a shared array
  
      let contentUrls = [];
  
      function wikilinkGenerator(label) {
        const name = slugify(label);
  
        for (const item of contentUrls) {
          if (item.id === name) {
            return item.url || label;
          }
        }
        return label;
      }
  
      // Base HTML template
      const baseTemplate = fs.readFileSync(config.baseTemplate, 'utf-8');
  
      // Log
      console.log("\x1b[36m", "Mapping out content...", "\x1b[0m");
  
      // Map out the content
      const contentMap = recursiveReaddir('./src/content')
      .map((filePath) => {
        const mimeType = mime.lookup(filePath);
  
        const file = {
          path: "",
          metadata: {},
          raw: "",
          template: "",
          frontmatter: ""
        };
  
        // Read Markdown and text files
        if(mimeType === "text/markdown" || mimeType === "text/plain") {
          file.raw = fs.readFileSync(filePath, "utf8");
  
          const fileParts = file.raw.split("---"); // Split at frontmatter delimiters
  
          if (fileParts.length > 2) {
            file.frontmatter = fileParts[1];
            file.metadata = parseFrontmatter(file.frontmatter) || {}; // Parse the frontmatter
            file.raw = fileParts[2];
          }
        }
  
        // Prepare file path
        const [_, fileRoute = ""] = filePath
          .replace(/\\/g, '/')
          .toLowerCase()
          .split('src/content/');
  
        // If no "template" is set in frontmatter, use default
        if(!file.metadata.template) {
          file.metadata.template = "default";
        }
        
        // If no "name" is set in frontmatter, generate from filename
        if (!file.metadata.name) {
          const fileNameParts = fileRoute.split("/");
          const fileName = fileNameParts.pop() || "";
          file.metadata.name = fileName.split(".")[0];
        }
  
        // Create the appropriate file path to /dist
        let url = fileRoute.split(".")[0];
        
        // Add the page to the shared array for wikilinks and sitemap
        contentUrls.push({
          name: file.metadata.name,
          id: slugify(file.metadata.name),
          url: "/" + url.replace("index", "")
        });
        
        // Finish constructing the file path
        if (fileRoute.indexOf("index") < 0 && fileRoute !== "404.md") {
          url = url + "/index";
        }
        file.path = `dist/${url}.html`;
  
        return file;
      });
  
      // Log
      console.log("\x1b[34m", "Compiling content pages...", "\x1b[0m");
  
      // Function to sort the content URLs alphabetically
      function urlSort(a, b) {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
  
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      };
  
      contentUrls = contentUrls.sort(urlSort);
  
      // Build the content pages
      contentMap.map((content) => {
        // Render the markdown
        const html = renderMarkdown(content.raw, contentUrls);
  
        // Build the context
        const context = {
          ...config, // Variables in config.json
          ...content.metadata, // Page metadata
          components: {}, // Components will be stored here
          content: html, // Rendered markdown content
          contentUrls, // Pages info for wikilinks
          cssPath, // Path to the CSS file
          lastUpdated, // Date of this build
          productionMode: buildMode === "prod"
        };
  
        // Build the components
        recursiveReaddir('./src/components')
          .forEach((componentPath) => {
            const component = {
              name: "",
              content: ""
            };
            
            // Render the component using whiskers templating and the context we built earlier
            component.content = renderTemplate(
              fs.readFileSync(componentPath, 'utf-8'),
              context
            );
  
            component.name = componentPath
              .replace("src\\components\\", "")
              .replace(".html", "");
            
            // Store the component in the context
            context.components[component.name] = component.content;
          });
  
        // Render the correct content template ()
        context.markup = renderTemplate(fs.readFileSync(`./src/templates/${context.template}.html`, "utf8"), context);
  
        // Render the base template
        const output = renderTemplate(baseTemplate, context);
  
        // Write the file
        fs.mkdir(path.dirname(content.path), {recursive: true}, (err) => {
          if (err) throw err;
          fs.writeFileSync(content.path, output);
        });
  
      });
  
      // Copy /public files into /dist
      console.log("\x1b[35m", "Copying files from /public...", "\x1b[0m");
  
      recursiveReaddir('./public')
      .map((file, index) => {
  
        let [_, filePath] = file
          .replace(/\\/g, '/')
          .toLowerCase()
          .split('public/');
        
        filePath = "dist/" + filePath;

        fs.mkdir(path.dirname(filePath), {recursive: true}, (err) => {
          if (err) throw err;

          fs.copyFile(file, filePath, (file_err) => {
            if (file_err) {
              console.warn("\x1b[31m", `Could not copy file: ${file_err}`, "\x1b[0m");
            }
          });
        });
      });

      cb();
  });
}