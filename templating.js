// Function to render templates

export default function renderTemplate(input, context) {

    function checkConditions(match, ...args) {

        const hasNamedGroups = typeof args.at(-1) === "object";
        
        if(hasNamedGroups) {
            if (context[args.at(-1).if]) {
                return renderTemplate(args.at(-1).ifcontent, context);
            }
            else if (args.at(-1).elsecontent) {
                return renderTemplate(args.at(-1).elsecontent, context);
            }
            else {
                return "";
            }
        }

        return renderTemplate(`IF ELSE OOPSIE "${match}"`, context);
    }

    function processLoopItem(name, input, item) {

        const itemRegex = new RegExp(`(?:{${name}\.(.*?)})`, "gsm");

        let output = input.replace(itemRegex, (match, p1) => {
            
            if(item[p1]) {
                return item[p1];
            }

            return "";
        });

        return output;
    
    }

    function loop(match, ...args) {

        const hasNamedGroups = typeof args.at(-1) === "object";
        const offset = hasNamedGroups ? args.at(-3) : args.at(-2);
        
        if(hasNamedGroups && args.at(-1).for) {
            const iterator = args.at(-1).for.split(" in ");
            let output = "";
            
            if(context[iterator[1]] && context[iterator[1]].length > 0) {
                context[iterator[1]].forEach((item) => {
                    const itemOutput = processLoopItem(iterator[0], args.at(-1).forcontent, item);
                    output += itemOutput;
                });
            }

            return output;
        }

        return `FOR OOPSIE "${match}"`;
    }
    
    function importTemplate(match, p1) {

        if(p1 && p1.startsWith(">")) {
            const thing = p1.slice(1).split('.').reduce((o,i)=> {
                if (o) {
                    return o[i];
                }
                return null;
            }, context);

            if(thing) {
                return renderTemplate(thing, context);
            }
        }
        else if (p1) {
            const thing = p1.split('.').reduce((o,i)=> {
                if (o && i) {
                    return o[i];
                }
                return null;
            }, context);

            if(thing) {
                return renderTemplate(thing);
            }
        }

        return renderTemplate(p1, context);
    }

    let output = input;

    const conditionalRegex = /{if (?<if>.+?)}(?<ifcontent>.+?)(?:{else if (.+?)}(.+?))*(?:{else}(?<elsecontent>.+?))?{\/if}/gsm;
    const loopRegex = /{for (?<for>.+?)}(?<forcontent>.+?){\/for}/gsm;
    const variableRegex = /(?:{(.+?)})/gm;

    output = output.replace(conditionalRegex, checkConditions);
    output = output.replace(loopRegex, loop);
    output = output.replace(variableRegex, importTemplate);

    return output;
}