# njoqi-wiki

![njoqi logo](project-logo.png "njoqi")

Website accessible at [njoqi.me](https://njoqi.me).

Personal portfolio/wiki, built with a custom flavour of [Markdown](https://www.markdownguide.org/) as a static website.

Deploys on [Codeberg Pages](https://codeberg.page/) through git.

## Development

1. `npm install` to install dependencies
2. `npm run watch` to build styles, and watch and rebuild content files in `src/components`, `src/content` and `src/templates`

The website is served from /dist on `localhost:8000`.

Routes are automatically defined through file structure in `src/content`. For instance, `src/content/make/index.md` will be available at `{url}/make`.

Any `.md` or `.txt` file in `src/content` is built into an html file. The template used can be defined in the page frontmatter.

Reusable components are placed in `src/components`. Assets such as images or fonts should go in the `public` folder.

## Build

Simply run `npm run build`.

The static website is generated in `/dist`.

The css bundle is minimized, and hashed for cache busting.

## Improvements

[ ] Fix CSS only building once

## (Un)license

This repository's code is [unlicensed](UNLICENSE). Texts, photos and other media content are [CC0](https://creativecommons.org/publicdomain/zero/1.0/) unless specified otherwise.