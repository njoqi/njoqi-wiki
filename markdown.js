// Function to render Markdown content

const imgRegex = /(?:!\[(.*?)]\((.+?)\)(?:{(.+?)})?)/gsm;
const linkRegex = /(?:\[([^[]*?)]\((.+?)\)(?<attr>{[^\.]+?=".+?"})?(?<classname>{\..+?})?)/gsm;
const wikilinkRegex = /(?:\[\[([^[]*?)]])/gsm;
const markRegex = /(?:==(.+?)==)/gsm;
const smallRegex = /(?:--(.+?)--)/gsm;
const listRegex = /(?:(?:\r?\n)?^- .+(?:[\r\n]{1,2}|$))+({\..+?})?/gm;
const blockquoteRegex = /(?:(?:\r?\n)?^> (?:.+(\r?\n))+)/gm;
const pRegex = /(?:^(?!<\/?[a-z=\-_" ]*?>|#{1,3} ).+(?:(?:\r?\n){1}|$))+/gm;
const h3Regex = /(?:#{3} )(.*)/g;
const h2Regex = /(?:#{2} )(.*)/g;
const h1Regex = /(?:#{1} )(.*)/g;
const dlRegex = /(?:.+?\r?\n: .+?(?:\r?\n){1,2})+/gm;

function replaceImg(match, ...args) {
	let dimensions = "";

	if(args.length > 4) {
		args[2].split(" ").forEach((dimension) => {
			let d = dimension.split("=");
			dimensions = dimensions + d[0] + "='" + d[1] + "' "
		});
	}

	return `<img alt="${args[0]}" src="${args[1]}" ${dimensions}/>`;
}

function replaceLink(match, ...attrs) {
	let attributes = "";
	
	if(attrs[6] && attrs[6].classname) {
		attributes += ` class="${attrs[6].classname.slice(2, -1)}"`;
	}

	if(attrs[6] && attrs[6].attr) {
		attributes += ` ${attrs[6].attr.slice(1, -1)}`;
	}

	return `<a ${attributes} href="${attrs[1]}">${attrs[0]}</a>`;
}

function replaceWikilink(input, wikilinks) {

	return input.replace(wikilinkRegex, (match, p1) => {
		const parts = p1.split("|");

		if (parts.length > 1) {
			return `<a href="${parts[0]}">${parts[1]}</a>`;
		}

		const link = wikilinks.find((l) => l.name && l.name.toLowerCase() === p1.toLowerCase());

		if (link) {
			return `<a href="${link.url}">${p1}</a>`;
		}

		return match;
	});
}

function replaceMark(match, p1) {
	return `<mark>${p1}</mark>`;
}

function replaceSmall(match, p1) {
	return `<small>${p1}</small>`;
}

function replaceList(match, ...args) {

	let list = match.split(/\r?\n- /);
	let className = "";

	if(args.length > 2 && args[0]) {
		className = ` class="${args[0].slice(2, -1)}"`;
		list[list.length - 1] = list[list.length - 1].split(/\r?\n/)[0];
	}

	let output = `\n<ul${className}>`;	

	list.forEach((item) => {
		if (item) {

			let classes = item.match(/{\..+?}(?:\r?\n)?/gi);

			if (classes && item.endsWith(classes[classes.length - 1])) {
				className = classes[classes.length - 1].replace(/(?:\r?\n)/g, () => "").slice(2, -1)
				output += `\n<li class="${className}">${item.slice(0, -classes[classes.length - 1].length)}</li>`;
			}
			else {
				output += `\n<li>${item}</li>`;
			}
		}
	})

	output += "\n</ul>";

	return output;
}

function replaceP(match) {
	let className = "";
	let output = match;

	const classes = match.match(/{\..+?}(?:\r?\n)?/gi);

	if (classes && match.endsWith(classes[classes.length - 1])) {
		className = ` class="${classes[classes.length - 1].replace(/(?:\r?\n)/g, () => "").slice(2, -1)}"`
		output = output.slice(0, -classes[classes.length - 1].length);
	}

	if (output.endsWith("\n")) {
		output = output.slice(0, -1);
	}
	if (output.endsWith("\r")) {
		output = output.slice(0, -1);
	}

	return `\n<p${className}>${output}</p>`;
}

function replaceBlockquote(match) {
	let output = "";
	let parts = match.split(/\r?\n/);

	if (!parts[0]) {
		parts.shift();
	}
	parts[0] = parts[0].slice(2);

	parts.forEach((part) => {
		if (part) {
			output += `\n<p>${part}</p>`;
		}
	});

	return `\n<blockquote>${output}</blockquote>`;
}

function replaceH3(match, p1) {
	return `<h3>${p1}</h3>`;
}

function replaceH2(match, p1) {
	return `<h2>${p1}</h2>`;
}

function replaceH1(match, p1) {
	return `<h1>${p1}</h1>`;
}

function replaceDl(match) {
	const parts = match.split(/\r?\n\r?\n/);
	let output = "";

	parts.forEach((part) => {
		if (part) {
			const halves = part.split(": ");
			output += `<dt>${halves[0]}</dt><dd>${halves[1]}</dd>`;
		}
	});

	return `<dl>${output}</dl>\n`;
}

// Parse Markdown into an HTML String.
export default function renderMarkdown(md, wikilinks) {
	let output = md;

	// Blocks
	output = output.replace(listRegex, replaceList);
	output = output.replace(dlRegex, replaceDl);
	output = output.replace(blockquoteRegex, replaceBlockquote);
	output = output.replace(pRegex, replaceP);
	output = output.replace(h3Regex, replaceH3);
	output = output.replace(h2Regex, replaceH2);
	output = output.replace(h1Regex, replaceH1);

	// Elements
	output = output.replace(imgRegex, replaceImg);
	output = output.replace(linkRegex, replaceLink);
	output = replaceWikilink(output, wikilinks);
	output = output.replace(markRegex, replaceMark);
	output = output.replace(smallRegex, replaceSmall);
	
	return output.replace(/^\n+|\n+$/g, '');
}
