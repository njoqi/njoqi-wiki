// DEV - Watch source files and rebuild as necessary
import fs from "fs";
import chokidar from "chokidar";
import build from "./builder.js";
import initServer from "./server.js";

// Bind the console to "log"
const log = console.log.bind(console);

// Log the watch start
log("\x1b[34m", "Preparing to watch...", "\x1b[0m");

// Load the configuration file from config.json
function loadConfig() {
  let output = JSON.parse(fs.readFileSync("./config.json", "utf8"));
  output.sourceDirs.push("./config.json", "./public");
  return output;
}

let config = loadConfig();

// Initiate file watch
function initWatcher() {

  log('Initialising watcher...');
  
  // Setup watcher
  const output = chokidar.watch(config.sourceDirs, {
    ignored: /(^|[\/\\])\../, // Ignore dotfiles
    persistent: true
  });

  // Add event listeners
  output
  .on('ready', () => {
    build(config,
      () => {
        log('Initial scan complete. Ready for changes.');
      },
      "dev"
    );
    
    output.on('add', path => {
      log(`File ${path} has been added.`);
      build(config, () => {}, "dev");
    })
    .on('change', path => {
      if (path === "config.json") { // If config.json was modified, reload the watcher
        log("\x1b[34m", "Configuration JSON has changed. Reloading watcher...", "\x1b[0m");
        output.close().then(() => {
          config = loadConfig();
          watcher = initWatcher();
        })
      }
      else {
        log(`File ${path} has been changed.`);
        build(config, () => {}, "dev");
      }
    })
    .on('unlink', path => {
      log(`File ${path} has been removed.`);
      build(config, () => {}, "dev");
    });
  });

  return output;
}

const watcher = initWatcher();
const server = initServer(parseInt(config.serverPort));

// Function to close all watchers and server instance
function terminate() {
  watcher.close()
    .then(() => server.watcher.close())
    .then(() => {
      server.instance.close();
      process.exit()
    });
}

// Try to quit gracefully on CTRL-C / CMD-C
process.on('SIGINT', function() {
  terminate();
});

process.on('SIGTERM', () => {
  terminate();
});