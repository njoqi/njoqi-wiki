// SERVER - Serve the website from /dist and send reload events
import chokidar from "chokidar";
import { readFile, accessSync, constants } from "fs";
import { createServer } from "http";
import { join, normalize, resolve, extname } from "path";

// Bind the console to "log"
const log = console.log.bind(console);

const types = {
  html: 'text/html',
  css: 'text/css',
  js: 'application/javascript',
  png: 'image/png',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg',
  gif: 'image/gif',
  json: 'application/json',
  svg: 'image/svg+xml',
  woff: 'font/woff',
  woff2: 'font/woff2',
};

// Setup root folder for static files
const staticRoot = normalize(resolve("./dist"));

// Function to start the server and watch for files
export default function initServer(port) {

  let client = null;

  const instance = createServer((req, res) => {
    // Log request
    log(`${req.method} ${req.url}`);
  
    const extension = extname(req.url).slice(1);
    const type = extension ? types[extension] : types.html;
    const supportedExtension = Boolean(type);
    
  
    // Return 404 if the extension is not supported
    if (!supportedExtension) {
      res.writeHead(404, { 'Content-Type': 'text/html' });
      res.end('404: File not found');
      return;
    }
  
    let fileName = req.url;
    
    // Root html
    if (req.url === '/') {
      fileName = 'index.html';
    }
    // Keep connection alive for live reload
    else if (req.url === "/subscribeReload") {

      res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache'
      });

      // Send response
      res.write('Subscribed to reload events.');

      // Keep the reference to the response to send events
      client = res;

      // Clear the reference when the connection is closed
      req.on('close', () => { client = null; });
      return;
    }
    else if (!extension) {
      // Serve /example.html if possible
      try {
        accessSync(join(staticRoot, req.url + '.html'), constants.F_OK);
        fileName = req.url + '.html';
      } catch (e) {
        // Otherwise serve /example/index.html
        fileName = join(req.url, 'index.html');
      }
    }
  
    const filePath = join(staticRoot, fileName);
    // Check the file accessed is not outside of our root folder
    const isPathUnderRoot = normalize(resolve(filePath)).startsWith(staticRoot);
  
    if (!isPathUnderRoot) {
      res.writeHead(404, { 'Content-Type': 'text/html' });
      res.end('404: File not found');
      return;
    }
  
    readFile(filePath, (err, data) => {
      if (err) {
        res.writeHead(404, { 'Content-Type': 'text/html' });
        res.end('404: File not found');
      } else {
        res.writeHead(200, { 'Content-Type': type });
        res.end(data);
      }
    });
  });
  
  instance.listen(port, () => {
    log(`Server running on port ${port}.`)
  });

  // Refresh event
  function sendRefresh() {
    if (client) {
      client.write("data: reload\n\n");
    }
  }

  // Watch files in /dist and refresh browser on changes
  const watcher = chokidar.watch("dist", {
    persistent: true
  })
  .on('ready', () => {
    watcher.on("add", () => {
      sendRefresh();
    })
    .on("change", () => {
      sendRefresh();
    })
    .on("unlink", () => {
      sendRefresh();
    })
  });

  return {instance, watcher};
}