---
name: Human History
title: Human History
meta:
  - name: description
    content: How humans have lived and changed.
---

<header class="intro">

# Human History

Knowing the rich tapestry of humanity's past, learning how fellow humans spent their lives before us on Earth: what a privilege!

</header>

## General

- [Going Medieval](https://going-medieval.com/)

## Ancient lifestyles

- [How one modest shrub entrapped humans in its service](https://aeon.co/essays/how-one-modest-shrub-entrapped-humans-in-its-service)
- [What hunter-gatherers demonstrate about work and satisfaction](https://aeon.co/essays/what-hunter-gatherers-demonstrate-about-work-and-satisfaction)
- [What was it like to grow up in the last Ice Age?](https://aeon.co/essays/what-was-it-like-to-grow-up-in-the-last-ice-age)

## Conflicts and oppressions

- [The idea of a ‘precolonial’ Africa is theoretically vacuous, racist and plain wrong about the continent’s actual history](https://aeon.co/essays/the-idea-of-precolonial-africa-is-vacuous-and-wrong)

## Gender

- [Hymn to Inanna](https://www.worldhistory.org/article/2109/hymn-to-inanna/)
- [Ancient Mesopotamian Transgender and Non-Binary Identities](https://www.academuseducation.co.uk/post/ancient-mesopotamian-transgender-and-non-binary-identities)