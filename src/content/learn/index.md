---
name: Learn
title: Learn
meta:
  - name: description
    content: Learning is a lifelong process.
---

<header class="intro">

# Learn

Learning is a lifelong process.

</header>

 I think staying curious and open-minded makes living a richer and more rewarding experience. Whether it's about [animal species](https://britishseafishing.co.uk/king-of-herrings/), [architectural history](https://en.wikipedia.org/wiki/Chinese_architecture), or [sociological issues](https://en.wikipedia.org/wiki/Revolting_Prostitutes), I enjoy knowing and understanding more about our world.

Here I regroup some resources and thoughts about subjects I like learning about.

- [![](/assets/images/learn/card-nature.png){height=200 width=400}<span>Nature</span>](/learn/nature)
- [![](/assets/images/learn/card-design.png){height=200 width=400}<span>Design</span>](/learn/design)
- [![](/assets/images/learn/card-sustainability.png){height=200 width=400}<span>Sustainability</span>](/learn/sustainability)
- [![](/assets/images/learn/card-human-history.png){height=200 width=400}<span>Human History</span>](/learn/human-history)
{.card-list}