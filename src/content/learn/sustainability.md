---
name: Sustainability
title: Sustainability
meta:
  - name: description
    content: Some ways we can lessen our impact on the planet.
---

<header class="intro">

# Sustainability

As ecological disasters are surrounding us and climate crisis keeps getting more serious, I find myself compelled to learn about the ways humans can live in smarter, more ecologically conscious and ethical ways.

Look into [[principles]] for some ways I try to limit my ecological impact.

</header>

## Degrowth and systemic change

- [Green growth](https://unevenearth.org/2022/10/green-growth/)
- [Utopia, not futurism: Why doing the impossible is the most rational thing we can do](https://unevenearth.org/2019/10/bookchin_doing_the_impossible/)
- [Unearthed](https://meanjin.com.au/essays/unearthed/)
- [Dark Ecology](https://orionmagazine.org/article/dark-ecology/)

## Agriculture and ecosystems

- [Human dominance is a fact, not a debate](https://aeon.co/essays/human-dominance-is-a-fact-not-a-debate)
- [Permaculture and the Myth of Overpopulation](https://frjohnpeck.com/permaculture-and-the-myth-of-overpopulation/)
- [The return of silvopasture](https://aeon.co/essays/heres-to-reviving-the-ancient-practice-of-silvopasture)
- [The Lost Forest Gardens of Europe](https://www.resilience.org/stories/2020-10-08/the-lost-forest-gardens-of-europe/)
- [Reweaving the Wild](https://aeon.co/essays/how-to-fix-the-disaster-of-human-roads-to-benefit-wildlife)
- [Globalisation lessens our world, but we do have alternatives](https://aeon.co/essays/globalisation-lessens-our-world-but-we-do-have-alternatives)
- ["Cursed be the ground"](https://www.resilience.org/stories/2025-02-06/cursed-be-the-ground/)

## Low Tech and permacomputing

- [LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com)
- [A question of "tech"](http://gauthierroussilhe.com/en/posts/une-erreur-de-tech)
- [Digital guide to low-tech](http://gauthierroussilhe.com/en/posts/convert-low-tech)
- [Permacomputing update 2021](http://viznut.fi/texts-en/permacomputing_update_2021.html)
- [Is techno-clutter ruining your life?](https://cheapskatesguide.org/articles/techno-clutter-farnell.html)

## Energy and pollution

- [Design life-cycle](https://www.designlife-cycle.com/)
- [The Waste Age](https://aeon.co/essays/ours-is-the-waste-age-thats-the-key-to-tranforming-the-future)

## Tools and products

- [The valtoare](https://andacod.com/blog/the-valtoare-a-natural-washing-machine), a natural washing machine
- [nowlight](https://deciwatt.global/), a manual power source
- [Billyboil](https://www.billyboil.com.au), an insulated slow-cooker making optimal use of initial cooking heat
- [Website Carbon Calculator](https://www.websitecarbon.com/), an online tool to check how energy-intensive a website is