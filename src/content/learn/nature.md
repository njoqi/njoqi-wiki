---
name: Nature
title: Nature
meta:
  - name: description
    content: Teachings about the world, and the life born in it.
---

<header class="intro">

# Nature

To understand the vastness of our world, from the smallest microbe to the largest cosmic event, is to know ourselves a little better.

</header>

## Biology

- [There is no such thing as a good or bad microbe](https://aeon.co/essays/there-is-no-such-thing-as-a-good-or-a-bad-microbe)
- [We need new metaphors that put life at the centre of biology](https://aeon.co/essays/we-need-new-metaphors-that-put-life-at-the-centre-of-biology)

## Consciousness, life and death

- [Why we must abandon the vegetative state diagnosis](https://aeon.co/essays/why-we-must-abandon-the-vegetative-state-diagnosis)
- [This Ciliate Is About to Die (video)](https://www.youtube.com/watch?v=ibpdNqrtar0)

## Relationships to nature

- [Tabletop RPGs as environmental texts](https://analoggamestudies.org/2022/09/tabletop-rpgs-as-environmental-texts/)