---
name: Links
title: Links
meta:
  - name: description
    content: Websites worth a visit once in a while.
---

<header class="intro">

# Links

A collection of websites I personally value.

</header>


## Friends

- [Orion Watt - Illustrator and comic artist](https://www.orionwatt.co.uk/)
- [Steakcy - Illustrator and comic artist](https://steakcy.com/portfolio/)
- [Dook - Designer](https://d00k.net/)

## Artists

- [fragment scenario - Mixed media](http://www.fragmentscenario.com/)
- [paupowpow - Digital graphics](https://paupowpow.neocities.org/)

## Nice stuff

- [Aeon - Articles about sciences, philosophy, culture...](https://aeon.co/)