---
name: Principles
title: Principles
meta:
  - name: description
    content: Life ethics and principles I try to follow.
---

<header class="intro">

# Principles

I strive to do things right during my time on Earth. This is a lifelong commitment that builds on experience, and bases itself on principles. These rules are not absolute nor set in stone, as my beliefs and ways of interacting with the world evolve with time, encounters and reflection.

These are some examples of such principles.

</header>

## General

- ==Do no harm but take no shit==: minimise the harm, pain, damage you might inflict onto others, but stay mindful of your own boundaries and wellbeing when doing so.
- ==Your rights end where others' begin==: you have rights and should uphold them, but accept that coexisting with other people — or even species — means there are limits to some of these rights.
- ==No one is perfect, especially you==: everyone, including yourself, can be wrong and make mistakes. You need to show understanding, stay open to different experiences and opinions, accept you might be wrong and be ready to change.
- ==Human hands are meant to lift each other==: we are social creatures built on cooperation. Help people around you, value teamwork and communication, and do not be afraid to ask for help when you need it.
- ==Joy in the little things==: appreciate the mundane, be thankful for small moments of joy, find childlike wonder in your daily life.

## Sustainability

- ==Respect the world that saw you born==: humanity is part of the life on Earth. We have a responsibility to be mindful of how we exploit natural spaces and resources, to find the right balance between using our environment and preserving it.
- ==Live slowly, live humbly==: learn how to be happy and fulfilled with less consumer goods. Use what you already have, and take care of it. Accept that things might take a bit more time and effort.
- ==Local first==: try consuming local goods, in-season food, and avoid traveling far from home.
- ==Reduce, reuse, recycle==: consume less goods, use second-hand items when possible, borrow or lend instead of buying, give or resell instead of throwing away. Prefer durable goods you can reuse and repair. Avoid single-use products, plastics, and over-packaged goods. When throwing something, do so properly.
- ==Energy is precious==: try to use energy efficiently by limiting energy intensive travel like planes or individual cars, using blankets or hot water bottles instead of heating an entire house, privileging low-power devices, turning off appliances when not in use. Prefer manual tools when possible.