---
name: Tea
title: Tea
meta:
  - name: description
    content: Informations about tea.
---

<header class="intro">

# Tea

Informations about tea, including a log of a few teas I have tried. I seem to have a soft spot for earthy and grassy flavours.

</header>

## Resources

- [Regional banchas as ethnographic documents](https://japaneseteasommelier.wordpress.com/2018/03/19/regional-banchas-as-ethnographic-documents/)
- [Basics, knowledge, and experiments about sencha brewing](https://japaneseteasommelier.wordpress.com/2015/03/03/basics-knowledge-and-experiments-about-sencha-brewing/)
- [How to brew tea properly. Brewing methods in different countries.](https://tea-side.com/blog/how-to-brew-tea/)
- [List of Japanese teas and their terminology](https://atelierkintsugitea.art/pages/list-of-japanese-teas-terminology)
- [Trek to a remote Himalayan village where artisans craft teapots fit for kings](https://aeon.co/videos/trek-to-a-remote-himalayan-village-where-artisans-craft-teapots-fit-for-kings)

## Tea log

### Sencha

Origin
: Japan

Type
: green tea

Appearance
: fine, powdery

Profile
: vegetal

### Kasakura

Origin
: Japan

Type
: smoked tea (cherry tree wood)

Appearance
: shrivelled, crispy, tiny twigs

Profile
: smoky, raspy, rich

### Hōji Kukicha

Origin
: Japan

Type
: roasted tea stems

Appearance
: small twigs

Profile
: comforting, smoky, umami

### Kukicha

Origin
: Japan

Type
: green tea stems

Appearance
: fine, dark green leaves and light green stems

Profile
: vegetal, sweet

### Yunnan Black

Origin
: China

Type
: black tea

Appearance
: shrivelled, sometimes golden, few twigs

Profile
: smoky, sweet

### Imperial Jejudo

Origin
: Korea

Type
: black tea

Appearance
: small, twisted

Profile
: vegetal, umami

### Itoigawa Batabatacha

Origin
: Japan

Type
: mixed roasted tea (tea stems, soy beans, tea flowers, Chamaecrista nomame flowers)

Appearance
: twigs of various sizes, whole leaves, split beans

Profile
: toasted, cereals, cocoa

Notes
: meant to be consumed whipped with a hint of salt