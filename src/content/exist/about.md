---
name: About me
title: About me
meta:
  - name: description
    content: Information about me.
---

<header class="intro">

# About me

Hello there! Nice to meet you :)

</header>

My name is ==Meryl==. I'm 31 years old, and my pronouns are ==they/them==. I currently live in ==[Rennes, France](https://en.wikipedia.org/wiki/Rennes)==, where I work at home on various projects.

![An androgynous white person with wavy dark brown hair, round glasses and ear jewellery.](/assets/images/exist/meryl-2025.jpg){height=720 width=1200}

I'm a graphic designer & front-end developer! Currently working at ==[Atelier Joualle](https://joualle.eu)==, an engaged web & design studio with affordable pricing. I also completed a year learning [Social Care](https://www.nescol.ac.uk/courses/advanced-social-care/ "Providing care, protection and support to people") at the North East Scotland College.

I am a curious person whose interests span across:

- 📐 design, architecture
- 🎨 visual arts (esp. photography)
- 🌤 nature, fauna &amp; flora
- 🌿 sustainable living &amp; ecology
- 📕 social sciences
- 🎵 electronic music &amp; field recordings
- 🎮 video games and interactive media
- 💾 software development
- 📜 writing
- 🍜 food (mostly vegan!)

When I'm not jumping around in code, words, or drawings, you can usually find me drinking [[tea]], taking a stroll outdoors, growing [[/exist/garden|plants]] and/or taking [[/make/photography|photos]].

You can chat with me by email at ==[hello@njoqi.me](mailto:hello@njoqi.me){.mail-link}== or on Mastodon at ==[@njoqi@post.lurk.com](https://post.lurk.org/@njoqi){rel="me"}==!

## Also check out

- some interesting [[links]] on the web
- info about [[/make/njoqi|this website]]
- my collection of [[buttons]]