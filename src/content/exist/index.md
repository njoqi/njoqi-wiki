---
name: Exist
title: Exist
meta:
  - name: description
    content: Strive to find joy in existence.
---

<header class="intro">

# Exist

Strive to find joy in existence.

</header>

You'll find here some more personal pages, revolving around me and my everyday life.

- [![](/assets/images/exist/card-about.png){height=200 width=400}<span>About me</span>](/exist/about)
- [![](/assets/images/exist/card-principles.png){height=200 width=400}<span>Principles</span>](/exist/principles)
- [![](/assets/images/exist/card-food.png){height=200 width=400}<span>Food</span>](/exist/food)
- [![](/assets/images/exist/card-tea.png){height=200 width=400}<span>Tea</span>](/exist/tea)
- [![](/assets/images/exist/card-garden.png){height=200 width=400}<span>Garden</span>](/exist/garden)
- [![](/assets/images/exist/card-links.png){height=200 width=400}<span>Links</span>](/exist/links)
{.card-list}