---
name: Garden
title: Garden
meta:
  - name: description
    content: I grow plants.
---

<header class="intro">

# Garden

A non-exhaustive list of plants I have grown in my flat.

</header>

## Current garden
I currently have a cat who eats any plant he can grab... He's making things difficult.

- Alpine - ==Silver Fir== (Albies alba)
- Spaghebou & Bolobolo - ==Adanson's Monstera== (Monstera adansonii)
- Joy - ==Calla Lily "Lemon Drop"== (Zantedeschia elliotiana × maculata)
- Unnamed - ==Weeping Fig== (Ficus benjamina)

## Previous garden
- Emrakul - ==Polka-dot begonia== (Begonia maculata)
- Danger - ==Ti plant== (Coryline fruticosa)
- Artemis - ==Syngonium "Painted Arrow"== (Syngonium podophyllum)
- Cyberspace - ==Prayer plant "Fascinator"== (Maranta leuconeura)
- Coffee Des - ==Coffee Plant== (Coffea arabica)
- Pillow - ==Marimo moss ball== (Aegagropila linnaei)
- Claw - ==Air plant== (Tillandsia caput-medusae)
- Pipo - ==Chinese Money Plant== (Pilea peperomioides)
- Bobby - ==Mosses in a vial==
- Soap - ==Coriander== (Coriandrum Sativum)
- Plume - ==Parlour parlm== (Chamaedorea elegans)
- Unnamed - ==Small-leaf Spiderwort== (Tradescantia fluminensis)
- Unnamed - ==Banana "Tropicana"== (Musa acuminata)
- Unnamed - ==Avocado== (Persea americana)
- Unnamed - ==Japanese maple== (Acer palmatum)
- Unnamed - ==Australian chestnut== (Castanospermum australe)
- Unnamed - ==Philodendron "Birkin"== (Philodendron tatei) 
- Unnamed - ==Bridal wreath== (Stephanotis floribunda)
- Unnamed - ==Alocasia== (Alocasia longiloba)
- Unnamed - ==Parsley==
- Unnamed - ==Tomatoes==
- Unnamed - ==Gallant soldier / Guascas== (Galinsoga parviflora)
- Unknown plant, maybe pepper?
- Unknown plant, possibly Hyemale