---
name: Food
title: Food
meta:
  - name: description
    content: A few recipes
---

<header class="intro">

# Food

A few recipes I want to have logged somewhere. I tend to eat vegan when possible, both for ethical and ecological reasons.

</header>

## Recipes

### Green peas curry soup

- Olive oil
- 2 leeks
- 1kg frozen green peas
- 1L broth
- 3 teaspoons curry paste
- 400mL coconut milk
- 0.5 / 1 candied lemon
- Curry powder, lemon juice, salt/pepper to taste
- Fresh basil

Mince and brown the leeks in olive oil for a few minutes, in a large pot.
Add the peas, broth, curry, coconut milk.
Mince and add the candied lemon.
Bring to a boil, then let simmer until the peas are well cooked.
Adjust the seasoning.
Let cool down a bit, then blend.
Add thinly minced basil.
Serve hot or cold.

### Chili sin carne

- Olive oil
- 2 onions
- 2/3 tomatoes
- 1/2 carrots
- Tomato paste
- 500g can of red beans with its juice
- 1 teaspoon curry paste
- 1 pinch of garlic powder
- Cumin, chili, paprika powders
- Tabasco, lemon juice, salt to taste

Mince and brown the onions in olive oil for a few minutes, in a large pan.
Add diced tomatoes and carrots, let cook a bit.
Lower the heat.
Add the red beans with their juice. Mash some of them into the mixture to release their flavour.
Add tomato paste, the spices, and mix. Let simmer for a while, stirring occasionally.
Adjust spices and condiments to taste.
Serve with rice or use in burritos.

### Spiced potatoes

- Potatoes
- Onion
- Zucchini (optional)
- Olive oil
- Cumin, chili, cinnamon, ginger powders
- Salt and pepper
- Lemon juice
- Honey
- Chutney
- Cilantro (optional)

Peel potatoes, cut into large pieces.
Boil until just cooked.
Let cool a little.
Heat a generous amount of olive oil. Add the potatoes. You can add thinly sliced zucchini.
Add the spices, salt and pepper, lemon juice and honey.
Fry until golden.
Towards the end add very thinly sliced onion.
Serve with chutney and cilantro.

### Simple pancakes

- 300g wheat flour
- 12cl neutral oil
- 1 chemical yeast bag
- 2 teaspoons of sugar
- 2 pinches of salt
- milk

Mix all the dry ingredients in a large bowl.
Add the oil and some milk, stir well.
Continue mixing while adding milk until the dough is liquid enough.
Cook in a slightly oiled pan on medium/high heat, on each side (a side should cook in about 10-15 seconds).
