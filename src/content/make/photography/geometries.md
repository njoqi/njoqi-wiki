---
name: Géométries
title: Géométries
meta:
  - name: description
    content: Masses, shapes and patterns of fabricated landscapes.
---

<header class="intro">

# Géométries

Masses, shapes and patterns of fabricated landscapes.

</header>

[← Photography](/make/photography){.button} {.breadcrumb}

- ![](/assets/images/make/photography/geometries/g_08.jpg){height=900 width=1200} {.photograph}
- ![](/assets/images/make/photography/geometries/g_07.jpg){height=900 width=1200} {.photograph}
- ![](/assets/images/make/photography/geometries/g_06.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_05.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_04.jpg){height=690 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_03.jpg){height=690 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_02.jpg){height=690 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_01.jpg){height=630 width=1200 loading=lazy} {.photograph}