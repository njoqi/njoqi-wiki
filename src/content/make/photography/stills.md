---
name: Stills
title: Stills
meta:
  - name: description
    content: Moments on Earth, suspended.
---

<header class="intro">

# Stills

Moments on Earth, suspended.

</header>

[← Photography](/make/photography){.button} {.breadcrumb}

- ![The edge of a lake bordered with reeds, and some woods in the background. A small red boat is lying against a tree](/assets/images/make/photography/stills/s_26.jpg){height=720 width=1200} {.photograph}
- ![A lush forest with a dirt path leading to a tree with four thin trunks, covered in bright green moss and peppered with bright red leaves](/assets/images/make/photography/stills/s_25.jpg){height=1600 width=1200} {.photograph}
- ![Part of a grassy hill disappearing into the fog, a single tall grass and a leafless tree standing out](/assets/images/make/photography/stills/s_24.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![A small wooden shed surrounded by bushes and trees swaying in the wind. A wind turbine stands in the background](/assets/images/make/photography/stills/s_23.jpg){height=1600 width=1200} {.photograph}
- ![Small water stream turning in a large grassy field. A thick forest lies in the background](/assets/images/make/photography/stills/s_22.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Misty forest with fallen leaves covering the floor](/assets/images/make/photography/stills/s_21.jpg){height=1400 width=1200} {.photograph}
- ![Dirt path leading into a large grassless area, bordered with wooden fences and fading into a deep fog](/assets/images/make/photography/stills/s_20.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Road leading into grassy hills, with moody skies and foggy light. A few rural houses and a wind turbine can be seen](/assets/images/make/photography/stills/s_19.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![A countryside road with a bin next to a speed sign. In the back, a couple of lone houses and a modern lighthouse stand in a large field](/assets/images/make/photography/stills/s_18.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Black and white valley with clouds contrasting against the mountains](/assets/images/make/photography/stills/s_17.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Black and white valley with three small lakes fading into the fog](/assets/images/make/photography/stills/s_16.jpg){loading=lazy height=1600 width=1200} {.photograph}
- ![Silhouettes of trees on a recline, fading into foggy clouds](/assets/images/make/photography/stills/s_15.jpg){loading=lazy height=1800 width=1200} {.photograph}
- ![Hazy wing of a plane with sunshine reflecting on it, fading into the clouds](/assets/images/make/photography/stills/s_14.jpg){loading=lazy height=1600 width=1200} {.photograph}
- ![Silhouettes of street lamps against an evening sky, with a large cloud in the middle](/assets/images/make/photography/stills/s_13.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Train tracks disappearing into a forest section, surrounded by distant mountains](/assets/images/make/photography/stills/s_12.jpg){loading=lazy height=900 width=1200} {.photograph}
- ![Water tower on the side of a hill, with trees lining up along the slope and a foggy background](/assets/images/make/photography/stills/s_11.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Industrial complex right in front of a lake, with cloudy mountains behind](/assets/images/make/photography/stills/s_10.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Black and white close-up of a rock formation, contrasting against a bright sky](/assets/images/make/photography/stills/s_09.jpg){loading=lazy height=800 width=1200} {.photograph}
- ![Building with a reflective dome hidden behind trees, with a river next to it and dramatic clouds](/assets/images/make/photography/stills/s_08.jpg){loading=lazy height=720 width=1200} {.photograph}
- ![Wooden fishing shed perched atop rocks, next to the ocean](/assets/images/make/photography/stills/s_07.jpg){loading=lazy height=1600 width=1200} {.photograph}
- ![Train power lines following a fence overgrown with vegetation](/assets/images/make/photography/stills/s_06.jpg){loading=lazy height=690 width=1200} {.photograph}
- ![A dead white butterfly resting in a pot full of dry plants](/assets/images/make/photography/stills/s_05.jpg){loading=lazy height=690 width=1200} {.photograph}
- ![Silhouette of a crane against a purple sunset sky](/assets/images/make/photography/stills/s_04.jpg){loading=lazy height=690 width=1200} {.photograph}
- ![Black and white front of a shed with a metal roof and wooden planks walls. A deer head figure is mounted on it](/assets/images/make/photography/stills/s_03.jpg){loading=lazy height=1200 width=1200} {.photograph}
- ![Ceiling mounted lamp reflecting an orange evening light coming from a nearby window](/assets/images/make/photography/stills/s_02.jpg){loading=lazy height=690 width=1200} {.photograph}
- ![Close-up of a train window and a red curtain, soft light coming from outside](/assets/images/make/photography/stills/s_01.jpg){loading=lazy height=900 width=1200} {.photograph}