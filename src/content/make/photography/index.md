---
name: Photography
title: Photography
meta:
  - name: description
    content: Selected photographic works.
---

<header class="intro">

# Photography

Visual memories.

</header>

Work
: Digital photography

Client
: Personal

Year
: 2014 - Present

## Themes

Note: the photos are listed in reverse chronological order (most recent first).

- [[Animals]]
- [[Humans]]
- [[Géométries]]
- [[Stills]]
- [[Intervalles]]

Here are some of my personal favourites:

- ![](/assets/images/make/photography/stills/s_23.jpg){height=1600 width=1200} {.photograph}
- ![](/assets/images/make/photography/intervalles/i_05.jpg){height=900 width=1200} {.photograph}
- ![](/assets/images/make/photography/animals/a_01.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/humans/h_03.jpg){height=800 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/geometries/g_03.jpg){height=690 width=1200 loading=lazy} {.photograph}