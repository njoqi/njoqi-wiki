---
name: Animals
title: Animals
meta:
  - name: description
    content: Fur, scales or feathers — creatures of lands and seas, whenever I manage to spot and approach them.
---

<header class="intro">

# Animals

Fur, scales or feathers — creatures of lands and seas, whenever I manage to spot and approach them.

</header>

[← Photography](/make/photography){.button} {.breadcrumb}

- ![](/assets/images/make/photography/animals/a_23.jpg){height=720 width=1200} {.photograph}
- ![](/assets/images/make/photography/animals/a_22.jpg){height=720 width=1200} {.photograph}
- ![](/assets/images/make/photography/animals/a_21.jpg){height=720 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_20.jpg){height=720 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_19.jpg){height=1600 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_18.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_17.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_16.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_15.jpg){height=800 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_14.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_13.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_12.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_11.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_10.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_09.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_08.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_07.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_06.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_05.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_04.jpg){height=900 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_03.jpg){height=800 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_02.jpg){height=690 width=1200 loading=lazy} {.photograph}
- ![](/assets/images/make/photography/animals/a_01.jpg){height=900 width=1200 loading=lazy} {.photograph}