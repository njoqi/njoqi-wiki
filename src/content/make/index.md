---
name: Make
title: Make
meta:
  - name: description
    content: Humans enjoy making things.
---

<header class="intro">

# Make

Humans enjoy making things.

</header>

One of the things that set us apart from [most animals](https://en.wikipedia.org/wiki/Tool_use_by_animals) is our ability to create.

I enjoy partaking in the joys of making and seeing what others make. One of my objectives in life is to [[learn]] more ways to create: music, videogame making, pottery, woodworking...

Here are a few things I work(ed) on.

- [![](/assets/images/make/card-photography.png){height=200 width=400}<span>Photography</span>](/make/photography)
- [![](/assets/images/make/card-earth-noise.png){height=200 width=400}<span>Earth/Noise</span>](/make/earth-noise)
- [![](/assets/images/make/card-njoqi.png){height=200 width=400}<span>njoqi</span>](/make/njoqi)
{.card-list}