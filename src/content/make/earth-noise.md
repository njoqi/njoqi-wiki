---
name: Earth/Noise
title: Earth/Noise
meta:
  - name: description
    content: Cli-fi story about being part of this world.
---

<header class="intro">

# Earth/Noise

The wind turbines sing in the fields.

</header>

Work
: Writing, illustration

Client
: Personal

Year
: 2019 - Present

## Synopsis

Sometime near the end of the 21st century, young engineer Elwan contemplates their future, makes difficult life choices, and uncovers dangerous corporate schemes with the help of a childhood friend, a mischievous grandma and that secretive girl who keeps coming to the family café.

## Themes

The story is meant to mix slice of life content with an overarching story questioning themes like water scarcity, climate change and the politics of energy availability. It also explores possible alternatives to our current ways of life regarding society and technology.

![Elwan, the main character, in a high-visibility work uniform.](/assets/images/make/earth-noise-elwan.jpg){height=1537 width=1400}