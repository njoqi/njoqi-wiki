---
name: Atelier Joualle
title: Atelier Joualle
meta:
  - name: description
    content: Web and design agency focused on social solidarity.
---

<header class="intro">

# Atelier Joualle

Web and design agency focused on social solidarity.

</header>

Work
: Logo/visual identity, web design, front-end development

Client
: Personal

Year
: 2024 - Present

## Concept

==[Atelier Joualle](https://joualle.eu)== is a web and design agency built to provide affordable, engaged services to small structures. It focuses on making good, simple identities and websites for:

- charities
- small, local businesses
- self-employed creators
- public services

## Technology

The website is built using [ProcessWire](https://processwire.com/), a small and flexible PHP CMS.

It is not finished yet!