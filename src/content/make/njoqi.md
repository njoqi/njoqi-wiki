---
name: njoqi
title: njoqi
meta:
  - name: description
    content: Information about this website.
---

<header class="intro">

# njoqi

The place you're currently visiting.

</header>

Work
: Logo/visual identity, web design, front-end development

Client
: Personal

Year
: 2021 - Present

## Concept

> “Digital gardens explore a wide variety of topics and are frequently adjusted and changed to show growth and learning, particularly among people with niche interests.”
— Tanya Basu, <cite>[Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/)</cite>

==njoqi== was inspired by personal wikis and digital gardens like Neauoire's [XXIIVV](https://wiki.xxiivv.com/), and aims to be:

- a photography portfolio
- a compendium of knowledge and useful resources
- a fun little bit of Internet I can play with

## Technology

It is generated via NodeJS using [Markdown](https://en.wikipedia.org/wiki/Markdown) files for content and HTML for templating. The script uses as little NPM modules as possible (for now 3 packages).

I aim to keep it accessible, have a [small footprint](https://www.websitecarbon.com/website/njoqi-me/), and a simple and user-friendly development stack. It is currently hosted on [Infomaniak](https://www.infomaniak.com/), with the code available on its own [Codeberg git repo](https://codeberg.org/njoqi/njoqi-wiki). I switched to Codeberg as an open-source, non-profit, Europe-based alternative to Github.

Ultimately, I want to try self-hosting it on a used low-power computer for several reasons:
- full control and ownership
- low power consumption
- possibly use renewable energy
- learning about server configuration
- in the future, turning the computer into a local community hosting station