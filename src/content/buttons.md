---
title: Buttons
name: Buttons
meta:
  - name: description
    content: My collection of retro HTML buttons.
---

<header class="intro">

# Buttons

A nod to old-school web.

</header>

Websites in the 90s and early 2000s often had these little [88x31 pixels](https://www.quora.com/How-did-the-odd-size-of-88x31-become-a-standard-for-a-website-button) buttons somewhere. Usually to link to other websites, or shout out to/promote something they liked.

Here's some I have made or collected so far.

[![njoqi](/assets/images/buttons/button-njoqi.png){height=31 width=88}](https://njoqi.me){.image-link}
![under construction](/assets/images/buttons/button-construction.png){height=31 width=88}
[![i love lichen](/assets/images/buttons/button-lichen.png){height=31 width=88}](https://en.wikipedia.org/wiki/Lichen){.image-link}
[![degrowth](/assets/images/buttons/button-degrowth.png){height=31 width=88}](https://en.wikipedia.org/wiki/Degrowth){.image-link}
![Sonic](/assets/images/buttons/button-sonic.png){height=31 width=88}
[![Minecraft](/assets/images/buttons/button-minecraft.png){height=31 width=88}](https://www.minecraft.net){.image-link}
![Queer pride](/assets/images/buttons/button-queer.png){height=31 width=88}
![Trans](/assets/images/buttons/button-trans.gif){height=31 width=88}
![Enby](/assets/images/buttons/button-enby.png){height=31 width=88}
![graphic design is my passion](/assets/images/buttons/button-graphic.png){height=31 width=88}
![The Digital Me](/assets/images/buttons/button-digitalme.png){height=31 width=88}
![Network Neighborhood](/assets/images/buttons/button-neighborhood.png){height=31 width=88}
![HTML](/assets/images/buttons/button-html.gif){height=31 width=88}
![CSS](/assets/images/buttons/button-css.png){height=31 width=88}
![Public Domain](/assets/images/buttons/button-publicdomain.png){height=31 width=88}
[![neocities.org](/assets/images/buttons/button-neocities.png){height=31 width=88}](https://neocities.org/){.image-link}
[![Mastodon](/assets/images/buttons/button-mastodon.png){height=31 width=88}](https://joinmastodon.org/){.image-link}
[![bandcamp](/assets/images/buttons/button-bandcamp.png){height=31 width=88}](https://bandcamp.com/){.image-link}
[![Internet Archive](/assets/images/buttons/button-internet-archive.gif){height=31 width=88}](https://archive.org/){.image-link}
[![Codeberg](/assets/images/buttons/button-codeberg.png){height=31 width=88}](https://codeberg.org/){.image-link}
![Made with Windows](/assets/images/buttons/button-windows.png){height=31 width=88}
![Responsive Website](/assets/images/buttons/button-responsive.png){height=31 width=88}
[![Get Firefox](/assets/images/buttons/button-firefox.png){height=31 width=88}](https://www.mozilla.org/en-GB/firefox/new/){.image-link}
[![uBlock Origin](/assets/images/buttons/button-ublock.png){height=31 width=88}](https://ublockorigin.com/){.image-link}
[![foobar 2000. plays music.](/assets/images/buttons/button-foobar2000.png){height=31 width=88}](https://www.foobar2000.org/){.image-link}
[![Visual Studio Code](/assets/images/buttons/button-vscode.png){height=31 width=88}](https://code.visualstudio.com/){.image-link}
{.unrounded}