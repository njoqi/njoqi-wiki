---
name: Homepage
title: njoqi
meta:
  - name: description
  - content: Personal website.
---

<header class="intro">

# Hello! Get comfy.

This is a little space I use to sort works, thoughts, links and resources that matter to me. Rest around the fire and take a look at what I want to record, display and share. Last updated: =={lastUpdated}==

</header>

![Illustration of two people warming up outside, around a log cabin campfire. One is sitting on a large rock. A tree with some mushrooms is on the left.](/assets/images/campfire.png){height=720 width=1200}

## Make

- [![](/assets/images/make/card-photography.png){height=200 width=400}<span>Photography</span>](/make/photography)
- [![](/assets/images/make/card-atelier-joualle.png){height=200 width=400}<span>Atelier Joualle</span>](/make/atelier-joualle)
- [![](/assets/images/make/card-earth-noise.png){height=200 width=400}<span>Earth/Noise</span>](/make/earth-noise)
- [![](/assets/images/make/card-njoqi.png){height=200 width=400}<span>njoqi</span>](/make/njoqi)
{.card-list}

## Learn

- [![](/assets/images/learn/card-nature.png){height=200 width=400}<span>Nature</span>](/learn/nature)
- [![](/assets/images/learn/card-design.png){height=200 width=400}<span>Design</span>](/learn/design)
- [![](/assets/images/learn/card-sustainability.png){height=200 width=400}<span>Sustainability</span>](/learn/sustainability)
- [![](/assets/images/learn/card-human-history.png){height=200 width=400}<span>Human History</span>](/learn/human-history)
{.card-list}

## Exist

- [![](/assets/images/exist/card-about.png){height=200 width=400}<span>About me</span>](/exist/about)
- [![](/assets/images/exist/card-principles.png){height=200 width=400}<span>Principles</span>](/exist/principles)
- [![](/assets/images/exist/card-food.png){height=200 width=400}<span>Food</span>](/exist/food)
- [![](/assets/images/exist/card-tea.png){height=200 width=400}<span>Tea</span>](/exist/tea)
- [![](/assets/images/exist/card-garden.png){height=200 width=400}<span>Garden</span>](/exist/garden)
- [![](/assets/images/exist/card-links.png){height=200 width=400}<span>Links</span>](/exist/links)
{.card-list}