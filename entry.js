// ENTRY - Build the website from Markdown and Whiskers templates
import fs from "fs";
import build from "./builder.js";

// Log the build start
console.log("\x1b[33m", "Now building...", "\x1b[0m");

// Load the configuration file from config.json
const config = JSON.parse(fs.readFileSync("./config.json", "utf8"));

build(config,
  () => {
    console.log("Build complete.");
  },
  process.argv.slice(2)[0] // Captures "-- dev" for development mode
);
